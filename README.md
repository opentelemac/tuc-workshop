# TUC: Git & GitLab Workshop


## Create a GitLab account and request access to the repisotory

To get started, you need to [register](https://gitlab.com/users/sign_in) on gitlab.com and [request access](https://gitlab.com/opentelemac/tuc-workshop/-/project_members/request_access) to the workshop repository.


## Workshop steps

- [ ] Clone the repository

- [ ] Create a new branch and switch on it

- [ ] Display all local branches and verify that you're on your own. You should see something like this:
```
  main
* my_branch
```

- [ ] Create a new text file with any content

- [ ] Use **git status** to verify that your new file is untracked

- [ ] Commit your file

- [ ] Open tuc.py, see that it is bugged and create an issue on GitLab about it

- [ ] Switch to the **main** branch and pull changes from the remote repository

- [ ] Switch back to your branch

- [ ] Fix tuc.py but don't commit it yet

- [ ] Use **git status** to check that you have modified a tracked file

- [ ] Rebase your branch on main and see that it does not work

- [ ] Stash your changes and do the rebase again: it should now work

- [ ] Recover your changes from the stash

- [ ] Commit your modified tuc.py

- [ ] Push your branch

- [ ] Open a Merge Request (MR) to merge your branch on main\
      The MR must close the issue automatically



